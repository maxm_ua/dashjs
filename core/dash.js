"use strict";
var instance = undefined;

class dashCLI {
   constructor() {
       this.handlers = this.extensions = {};
	   
	   this.locked = true;
	   this.delay = 150;
		
	   this.register({
	       exit : () => { process.exit(0) },
           help : () => { for (let v in this.handlers) dashCLI.CLI().echo(v) }
	   });
	   
	   require("fs").readdir("../ext", (err, files) => {
		  let name = file.replace("/(\.|js)/", dashCLI.CLI().empty);
		  
		  this.extensions[name] = require("../ext/" + file).load();
		  this.register(this.extensions[name].handlers());
		  
		  this.input();
	   });
   }
   
   input() {
       this.stdin = process.openStdin();
       
       this.stdin.addListener("data", (d) => {
		   if (this.locked) return; else {
				this.locked = true;
				
				dashCLI.CLI().echo();
           
				let raw = d.toString().trim().split(" "),
					data = [], command = raw[0];
               
				for (let i = 1; i < raw.length; i++)
					if (raw[i]) data.push(raw[i]);
           
				if (this.handlers.hasOwnProperty(command))
					this.handlers[command](data);
				else dashCLI.CLI().echo("invalid input");
           
				setTimeout(this.unlock, this.delay);
		   }
       });
       
       setTimeout(this.unlock, this.delay);
   }
   
   register(commands) {
       for (let name in commands) {
           if (!this.handlers.hasOwnProperty(name)) {
               this.handlers[name] = commands[name];
           } else {
               let oEvent = this.handlers[name];
               let nEvent = () => {
                   oEvent();
                   commands[name]();
               };
               this.handlers[name] = nEvent;
           }
       }
   }
   
	unlock() {
		dashCLI.CLI().echo(nl + "input : ");
		this.locked = false; 
	}
   
   static CLI() { return require("./cli.js") }
   
   static getInstance() {
       if (!instance) 
		   instance = new dashCLI();
       return instance;
   }
} 

exports.getInstance = dashCLI.getInstance;
dashCLI.CLI().echo("Core modules loaded");
