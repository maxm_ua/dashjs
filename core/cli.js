/****************************************************************************************
 * CLI - Base console output helper from dashjs/core.
 * Standalone node module. 
 *
 * @file core/cli.js
 * @author ValryanM
 * @version 1.0
 **/
"use strict";

var cli = {
	// Console special chars
    tab : "   ", nl : "\n", empty : "",
    
	// Basic output
    echo : (d) => { console.log( d || cli.empty ) },
	log : (s,id) => { cli.echo("[" + id + "] " + s) },
	
	// Print variable type and value
    vd : (v,d) => { return ( d ? "(" + typeof(v) + ")" : cli.empty ) + v; },
    
	// Dumps variable to stdout recursively.
    dump : (d,t) => {
        if (!t) t = cli.empty;
        
        switch (typeof(d)) {
            case "array" : 
                cli.echo(t + typeof(d) + " (")
                for (let v of d) 
                    cli.dump(v, t + cli.tab);
                cli.echo(t + ")");
            break;
            
            case "object" : 
                cli.echo(t + typeof(d) + " (");
                for (let v in d) switch (typeof(d[v])) {
					case "array" : case "object" :
						cli.echo(t + v + " : " + typeof(d[v]) + " (");
						cli.dump(d[v], t + cli.tab);
						cli.echo(t + ")");
					 break;
					 default : cli.echo(t + v+ " : " + cli.vd(d[v])); break;                     
                 }
                 cli.echo(t + ")");
             break;
             
             default : cli.echo(t + cli.vd(d)); break;
        }
    },
	
	clear : () => { process.stdout.write('\x1B[2J\x1B[0f') }
};

// Node module exports
for (var v in cli) exports[v] = cli[v];