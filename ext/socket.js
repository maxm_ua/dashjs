"use strict";
let cli = require("../core/cli.js");

class rawSocket {
    constructor(n) {
		this.id = n;
	}
    
    handlers() { return {
        connect : (p) => {
            this.client = require("net").createConnection(
                { host : p[0], port : p[1] }, 
                () => { cli.log("connected", this.id) }
            );
            
            this.client.on('data', (data) => {
               cli.log("data : " + data.toString(), this.id);
            });
            
            this.client.on('end', () => {
               cli.log("disconnected", this.id);
            });
        },
        
        disconnect : () => { this.client.end() },
        
        send : (p) => { this.client.write(p[0]) }
    } }
}

exports.load = (n) => { return new rawSocket(n) }
cli.echo("rawSocket extension loaded");