"use strict";
let cli = require("../core/cli.js");

class dashServer {
	constructor(n) {
		this.id = n;

		this.server = require("net").createServer((c) => {
			cli.log('client connected', this.id);
			c.on('end', () => {
				cli.log('client disconnected', this.id);
			});	

			c.write('hello' + cli.nl);
			c.pipe(c);
		});

		this.server.on('error', (err) => { throw err });
	}

	handlers() { return {
		start : (p) => { this.server.listen(p[0], () => {
			cli.log('listening on ' + p[0], this.id)
		}) },
		
		listen : () => {
			cli.log("listen " + this.server.address(), this.id)
		},
		
		stop : () => { this.server.close(() => {
			cli.log("stopped", this.id)
		}) }
	} }
}

exports.load = (n) => { return new dashServer(n) };
cli.echo("dashServer extension loaded");

