# README #

Application based on CLI environment. Allows you to rapidly add own commands/subprograms as dash extensions. 

### Version ###

* Version 1.1b
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Extension snippet ###

```
#!javascript
// Example (dummy) dash extension
"use strict";
let cli = require("../core/cli.js");

class dummyExtension {
    constructor() {}

    handlers() { return {
        // Extension commands
    } };
}

// Node.js module exports
exports.load = () => { return new dummyExtension() };
cli.echo("dummyExtension loaded");
```